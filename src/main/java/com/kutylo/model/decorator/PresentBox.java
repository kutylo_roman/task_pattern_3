package com.kutylo.model.decorator;


import com.kutylo.model.User;

public class PresentBox extends BouquetsDecorator {

    private final int ADDITIONAL_PRICE = 100;
    private final String TO_NAME = " + Present Box";

    public PresentBox(User user) {
        setAdditionalPrice(ADDITIONAL_PRICE);
        setToName(TO_NAME);
        user.setMoney(user.getMoney() - ADDITIONAL_PRICE);
    }

    @Override
    public String toString() {
        return "PresentBox{" +
                "ADDITIONAL_PRICE=" + ADDITIONAL_PRICE +
                ", TO_NAME='" + TO_NAME + '\'' +
                "}\n" + getBouquet();
    }
}
