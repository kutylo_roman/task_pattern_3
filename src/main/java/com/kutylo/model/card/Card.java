package com.kutylo.model.card;

public interface Card {
    int getEffect();
}
