package com.kutylo.model.card;

public class GoldenCard implements Card {

    int discount;

    public GoldenCard() {
        discount = 100;
    }

    @Override
    public int getEffect() {
        return discount;
    }

    @Override
    public String toString() {
        return "GoldenCard{" +
                "discount=" + discount +
                '}';
    }
}
