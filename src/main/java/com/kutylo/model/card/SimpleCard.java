package com.kutylo.model.card;

public class SimpleCard implements Card {
    int discount;

    public SimpleCard() {
        discount = 0;
    }

    @Override
    public int getEffect() {
        return discount;
    }

    @Override
    public String toString() {
        return "SimpleCard{" +
                "discount=" + discount +
                '}';
    }
}
