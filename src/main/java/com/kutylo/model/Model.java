package com.kutylo.model;

import com.kutylo.model.bouquets.Bouquet;
import com.kutylo.model.decorator.BouquetsDecorator;
import com.kutylo.model.decorator.Delivery;
import com.kutylo.model.decorator.PresentBox;
import com.kutylo.model.factory.BirthdayFactory;
import com.kutylo.model.factory.FuneralFactory;
import com.kutylo.model.factory.Salon;
import com.kutylo.model.factory.WeddingSalon;
import com.kutylo.model.card.SilverCard;

public class Model {
    User user;
    Bouquet bouquet;

    public Model() {
        user = new User("Maksym Korbiak", 2500, new SilverCard());
        bouquet = null;
    }

    public String getUserInfo() {
        return String.valueOf(user);
    }

    public void initUser(String name, int money) {
        user = new User(name, money);
    }

    public void createBouquet(int fIndex, int bIndex) {
        Salon salon = null;
        if (fIndex == 1) {
            salon = new WeddingSalon();
            if (bIndex == 1) bouquet = salon.assemble(Enum.RR, user);
            if (bIndex == 2) bouquet = salon.assemble(Enum.CS, user);
        } else if (fIndex == 2) {
            salon = new BirthdayFactory();
            if (bIndex == 1) bouquet = salon.assemble(Enum.G, user);
        } else if (fIndex == 3) {
            salon = new FuneralFactory();
            if (bIndex == 1) bouquet = salon.assemble(Enum.C, user);
        }
    }

    public void addComp() {
        BouquetsDecorator delivery = new Delivery(user);
        BouquetsDecorator presentBox = new PresentBox(user);
        delivery.setBouquet(bouquet);
        presentBox.setBouquet(delivery);

        bouquet = presentBox;

    }

    public String bouquetInfo() {
        if (bouquet == null) return "Null bouquet";
        return String.valueOf(bouquet);
    }
}
