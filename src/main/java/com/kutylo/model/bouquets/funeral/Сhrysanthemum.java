package com.kutylo.model.bouquets.funeral;

import com.kutylo.model.User;
import com.kutylo.model.bouquets.Bouquet;

import java.util.ArrayList;
import java.util.List;

public class Сhrysanthemum implements Bouquet {
    int price;

    List<String> comp;

    public Сhrysanthemum() {
        price = 950;
    }

    @Override
    public void writeOffs(User user) {

        user.setMoney(user.getMoney() - (price - user.getCard().getEffect()));
    }

    @Override
    public void prepare() {
        comp = new ArrayList<>();
        comp.add("15 Сhrysanthemum");
        comp.add("tapes");
    }

    @Override
    public void box() {
        comp.add("box");
    }

    @Override
    public List<String> getToppings() {
        return comp;
    }

    @Override
    public String toString() {
        return "Сhrysanthemum{" +
                "price=" + price +
                ", comp=" + comp +
                '}';
    }
}
