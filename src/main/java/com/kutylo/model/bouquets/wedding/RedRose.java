package com.kutylo.model.bouquets.wedding;

import com.kutylo.model.User;
import com.kutylo.model.bouquets.Bouquet;

import java.util.ArrayList;
import java.util.List;

public class RedRose implements Bouquet {

    int price;

    List<String> comp;

    public RedRose() {
        price = 750;
    }

    @Override
    public void writeOffs(User user) {
        user.setMoney(user.getMoney() - (price - user.getCard().getEffect()));
    }

    @Override
    public void prepare() {
        comp = new ArrayList<>();
        comp.add("15 roses");
        comp.add("tapes");
    }

    @Override
    public void box() {
        comp.add("box");
    }

    @Override
    public List<String> getToppings() {
        return comp;
    }

    @Override
    public String toString() {
        return "RedRose{" +
                "price=" + price +
                ", comp=" + comp +
                '}';
    }
}
