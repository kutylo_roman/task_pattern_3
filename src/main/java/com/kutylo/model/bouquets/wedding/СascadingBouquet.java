package com.kutylo.model.bouquets.wedding;

import com.kutylo.model.User;
import com.kutylo.model.bouquets.Bouquet;

import java.util.ArrayList;
import java.util.List;

public class СascadingBouquet implements Bouquet {

    int price;

    List<String> comp;

    public СascadingBouquet() {
        price = 1200;
    }

    @Override
    public void writeOffs(User user) {
        user.setMoney(user.getMoney() - (price - user.getCard().getEffect()));
    }

    @Override
    public void prepare() {
        comp = new ArrayList<>();
        comp.add("10 roses");
        comp.add("5 tulip");
        comp.add("tapes");
    }

    @Override
    public void box() {
        comp.add("box");
    }


    @Override
    public List<String> getToppings() {
        return comp;
    }

    @Override
    public String toString() {
        return "СascadingBouquet{" +
                "price=" + price +
                ", comp=" + comp +
                '}';
    }
}
