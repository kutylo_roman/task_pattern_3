package com.kutylo.model.bouquets;

import com.kutylo.model.User;

import java.util.List;

public interface Bouquet {

    void writeOffs(User user);

    void prepare();

    void box();

    List<String> getToppings();
}
