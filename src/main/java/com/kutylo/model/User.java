package com.kutylo.model;

import com.kutylo.model.card.Card;
import com.kutylo.model.card.SimpleCard;

public class User {
    String name;
    int money;
    Card card;

    public User(String name, int money, Card card) {
        this.name = name;
        this.money = money;
        this.card = card;
    }

    public User(String name, int money) {
        this.name = name;
        this.money = money;
        this.card = new SimpleCard();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", money=" + money +
                ", card=" + card +
                '}';
    }
}
