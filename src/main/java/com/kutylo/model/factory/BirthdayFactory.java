package com.kutylo.model.factory;

import com.kutylo.model.Enum;
import com.kutylo.model.bouquets.Bouquet;
import com.kutylo.model.bouquets.birthday.Gerberas;

public class BirthdayFactory extends Salon {


    @Override
    protected Bouquet create(Enum model) {
        Bouquet bouquet = null;

        if (model == Enum.G) {
            bouquet = new Gerberas();
        }

        return bouquet;
    }
}
