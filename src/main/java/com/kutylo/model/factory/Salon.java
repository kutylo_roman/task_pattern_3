package com.kutylo.model.factory;

import com.kutylo.model.Enum;
import com.kutylo.model.User;
import com.kutylo.model.bouquets.Bouquet;

public abstract class Salon {
    protected abstract Bouquet create(Enum model);

    public Bouquet assemble(Enum model, User user) {
        Bouquet bouquet = create(model);
        bouquet.writeOffs(user);
        bouquet.prepare();
        bouquet.box();
        return bouquet;
    }
}
