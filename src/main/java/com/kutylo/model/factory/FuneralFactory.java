package com.kutylo.model.factory;

import com.kutylo.model.Enum;
import com.kutylo.model.bouquets.Bouquet;
import com.kutylo.model.bouquets.funeral.Сhrysanthemum;

public class FuneralFactory extends Salon {
    @Override
    protected Bouquet create(Enum model) {
        Bouquet bouquet = null;

        if (model == Enum.C) {
            bouquet = new Сhrysanthemum();
        }

        return bouquet;
    }
}
