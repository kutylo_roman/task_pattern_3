package com.kutylo.model.factory;

import com.kutylo.model.Enum;
import com.kutylo.model.bouquets.Bouquet;
import com.kutylo.model.bouquets.wedding.RedRose;
import com.kutylo.model.bouquets.wedding.СascadingBouquet;

public class WeddingSalon extends Salon {

    @Override
    protected Bouquet create(Enum model) {
        Bouquet bouquet = null;

        if (model == Enum.RR) {
            bouquet = new RedRose();
        } if (model == Enum.CS){
            bouquet = new СascadingBouquet();
        }

        return bouquet;
    }
}
