package com.kutylo.view;

import com.kutylo.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class View {

    private Controller controller;
    private List<String> factory;
    private List<String> bouquetWedding;
    private List<String> bouquetFuneral;
    private List<String> bouquetBirthday;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(View.class);


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Init user");
        menu.put("2", "2 - User info");
        menu.put("3", "3 - Create Bouquet");
        menu.put("4", "4 - Bouquet info");
        menu.put("5", "5 - Add delivery and present box");
    }

    public View() {
        controller = new Controller();
        input = new Scanner(System.in);
        factory = new ArrayList<>();
        factory.add("Wedding");
        factory.add("Birthday");
        factory.add("Funeral");
        bouquetWedding = new ArrayList<>();
        bouquetWedding.add("RedRose");
        bouquetWedding.add("СascadingBouquet");
        bouquetBirthday = new ArrayList<>();
        bouquetBirthday.add("Gerberas");
        bouquetFuneral = new ArrayList<>();
        bouquetFuneral.add("Сhrysanthemum");
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::getEx1);
        methodsMenu.put("2", this::getEx2);
        methodsMenu.put("3", this::getEx3);
        methodsMenu.put("4", this::getEx4);
        methodsMenu.put("5", this::getEx5);

    }

    private void getEx5() {
        controller.addComp();
    }

    private void getEx4() {
        logger.info(controller.bouquetInfo());
    }

    private void getEx3() {
        logger.info(factory);
        logger.info("Choose salon:");
        int fIndex = input.nextInt();
        if (fIndex == 1) {
            logger.info(bouquetWedding);
        } else if (fIndex == 2) {
            logger.info(bouquetBirthday);
        } else if (fIndex == 3) {
            logger.info(bouquetFuneral);
        }
        logger.info("Choose bouquet:");
        int bIndex = input.nextInt();
        controller.createBouquet(fIndex, bIndex);
    }

    private void getEx2() {
        logger.info(controller.getUserInfo());
    }

    private void getEx1() {
        logger.info("Set name:");
        String name = input.nextLine();
        logger.info("Set money:");
        int money = input.nextInt();
        controller.initUser(name, money);
    }


    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).getCom();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }
}
