package com.kutylo.controller;

import com.kutylo.model.Model;


public class Controller {

    private Model model;

    public Controller() {
        this.model = new Model();
    }

    public void initUser(String name, int money) {
        model.initUser(name, money);
    }

    public String getUserInfo() {
        return model.getUserInfo();
    }

    public void createBouquet(int fIndex, int bIndex) {
        model.createBouquet(fIndex, bIndex);
    }

    public String bouquetInfo() {
        return model.bouquetInfo();
    }

    public void addComp() {
        model.addComp();
    }
}
